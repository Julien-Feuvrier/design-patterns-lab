package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.AttachCommand;
import eu.telecomnancy.sensor.Command;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.OffCommand;
import eu.telecomnancy.sensor.OnCommand;
import eu.telecomnancy.sensor.UpdateCommand;
import eu.telecomnancy.sensor.UpdateViewCommand;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * 
 * @author julien
 * the view with the command pattern
 *
 */
public class CommandSensorView extends JPanel implements View 
{
	// the commands used instead of the methods of the ISensor
	private Command onCommand, offCommand, updateCommand, attachCommand, updateViewCommand;
	
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    
    public CommandSensorView(ISensor c) 
    {
        this.onCommand = new OnCommand(c);
        this.offCommand = new OffCommand(c);
        this.updateCommand = new UpdateCommand(c);
        this.attachCommand = new AttachCommand(c, this);
        this.updateViewCommand = new UpdateViewCommand(c, value);
    	
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                onCommand.execute();
            }
        });

        off.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                offCommand.execute();
            }
        });

        update.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
            	updateCommand.execute();
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
        
        attachCommand.execute();
    }

	@Override
	public void update() 
	{
		updateViewCommand.execute();
	}
}