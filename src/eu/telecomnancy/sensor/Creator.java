package eu.telecomnancy.sensor;


/**
 * 
 * @author julien
 * interface used in the factory method pattern
 *
 */
public interface Creator 
{
	/**
	 * @brief creation of a default temperature sensor
	 * @return the new sensor created
	 */
	public ISensor FactoryMethod();
	
	/**
	 * @brief creation of a sensor
	 * @param sensorType : the type of the sensor the user wants to create
	 * @return the new sensor
	 */
	public ISensor FactoryMethod(String sensorType);
}
