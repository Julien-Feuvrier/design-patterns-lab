package eu.telecomnancy.sensor;


/**
 * 
 * @author julien
 * The command which apply on()
 * used in the command pattern for CommandSensorView
 *
 */
public class OnCommand implements Command 
{
	private ISensor sensor;
	
	public OnCommand(ISensor sensor)
	{
		this.sensor = sensor;
	}
	
	@Override
	public void execute() 
	{
		sensor.on();
	}
}
