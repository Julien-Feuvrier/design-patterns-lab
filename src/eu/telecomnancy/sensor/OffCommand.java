package eu.telecomnancy.sensor;


/**
 * 
 * @author julien
 * command which apply off() on the sensor
 *
 */
public class OffCommand implements Command 
{
	private ISensor sensor;
	
	public OffCommand(ISensor sensor)
	{
		this.sensor = sensor;
	}
	
	@Override
	public void execute() 
	{
		sensor.off();
	}
}
