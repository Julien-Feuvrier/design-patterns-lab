package eu.telecomnancy.sensor;


/**
 * 
 * @author julien
 * class using the decorator pattern in order to convert into Fahrenheit the value get by getValue
 *
 */
public class ConversionDecoratorTemperatureSensor extends DecoratorTemperatureSensor 
{
	public ConversionDecoratorTemperatureSensor()
	{
		super();
	}
	
	public ConversionDecoratorTemperatureSensor(ISensor sensor)
	{
		super(sensor);
	}
	
	@Override
	public double getValue() throws SensorNotActivatedException 
	{
		return sensor.getValue() * 9. / 5. + 32.;
	}
}
