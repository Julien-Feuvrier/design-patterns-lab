package eu.telecomnancy.sensor;

public class TemperatureSensorCreator implements Creator 
{
	public static final String DEFAULT_SENSOR = "DEFAULT";
	public static final String ADAPTER_SENSOR = "ADAPTER";
	public static final String CONVERSION_SENSOR = "CONVERSION";
	public static final String ROUND_SENSOR = "ROUND";
	public static final String PROXY_SENSOR = "PROXY";
	public static final String STATE_SENSOR = "STATE";
	
	@Override
	public ISensor FactoryMethod() 
	{
		return new TemperatureSensor();
	}

	@Override
	public ISensor FactoryMethod(String sensorType) 
	{
		sensorType = sensorType.toUpperCase();
		
		if (sensorType.equals(DEFAULT_SENSOR))
			return new TemperatureSensor();
		else if (sensorType.equals(ADAPTER_SENSOR))
			return new AdapterLegacyTemperatureSensor();
		else if (sensorType.equals(CONVERSION_SENSOR))
			return new ConversionDecoratorTemperatureSensor();
		else if (sensorType.equals(ROUND_SENSOR))
			return new RoundDecoratorTemperatureSensor();
		else if (sensorType.equals(PROXY_SENSOR))
			return new ProxyTemperatureSensor();
		else if (sensorType.equals(STATE_SENSOR))
			return new StateTemperatureSensor();
		else return this.FactoryMethod();
	}
}
