package eu.telecomnancy.sensor;

public class DesactivatedState implements State 
{
	public DesactivatedState()
	{
	}
	
	@Override
	public boolean getStatus() 
	{
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException 
	{
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");		
	}

	@Override
	public double getValue() throws SensorNotActivatedException 
	{
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}
}
