package eu.telecomnancy.sensor;

import java.util.Random;

public class ActivatedState implements State 
{
	private double value;
	
	public ActivatedState()
	{
		this.value = (new Random()).nextDouble() * 100;	
	}
	
	@Override
	public boolean getStatus()
	{
		return true;	// the sensor is activated		
	}

	@Override
	public void update() throws SensorNotActivatedException 
	{
		value = (new Random()).nextDouble() * 100;
	}

	@Override
	public double getValue() throws SensorNotActivatedException 
	{
		return value;
	}
}
