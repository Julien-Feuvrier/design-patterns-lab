package eu.telecomnancy.sensor;

import javax.swing.JLabel;

public class UpdateViewCommand implements Command 
{
	private ISensor sensor;
	private JLabel value;
	
	public UpdateViewCommand(ISensor sensor, JLabel value)
	{
		this.sensor = sensor;
		this.value = value;
	}
	
	@Override
	public void execute() 
	{
		if (sensor.getStatus())
		{
			try 
			{
				value.setText(sensor.getValue() + " °C");
			} 
			catch (SensorNotActivatedException e) 
			{
				e.printStackTrace();
			}
		}
		else 
			value.setText("N/A °C");
	}
}
