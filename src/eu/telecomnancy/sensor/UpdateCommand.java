package eu.telecomnancy.sensor;


/**
 * 
 * @author julien
 * execute sensor.update()
 * 
 */
public class UpdateCommand implements Command 
{
	private ISensor sensor;
	
	public UpdateCommand(ISensor sensor)
	{
		this.sensor = sensor;
	}
	
	@Override
	public void execute()
	{
		try
		{
			sensor.update();
		}
		catch (SensorNotActivatedException e)
		{
			e.printStackTrace();
		}
	}
}
