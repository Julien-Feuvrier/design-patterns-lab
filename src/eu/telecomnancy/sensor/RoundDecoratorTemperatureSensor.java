package eu.telecomnancy.sensor;


/**
 * 
 * @author julien
 * class using the decorator pattern in order to round the value get by getValue
 *
 */
public class RoundDecoratorTemperatureSensor extends DecoratorTemperatureSensor 
{
	public RoundDecoratorTemperatureSensor()
	{
		super();
	}
	
	public RoundDecoratorTemperatureSensor(ISensor sensor)
	{
		super(sensor);
	}

	@Override
	public double getValue() throws SensorNotActivatedException 
	{
		return Math.round(sensor.getValue());
	}

}
