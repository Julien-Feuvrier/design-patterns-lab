package eu.telecomnancy.sensor;

import java.util.ArrayList;
import eu.telecomnancy.ui.View;


/**
 * 
 * @author julien
 * @brief sensor using a state pattern
 *
 */
public class StateTemperatureSensor implements ISensor 
{
    private ArrayList<View> views;
    private State state;		// contains the state of the sensor (and the value)
    
    
    public StateTemperatureSensor()
    {
    	state = new DesactivatedState();	// the sensor is at the beginning desactivated
    	views = new ArrayList<View>();
    }
    
	@Override
	public void on() 
	{
		state = new ActivatedState();
		
		this.Notify();
	}

	@Override
	public void off() 
	{
		state = new DesactivatedState();
		
		this.Notify();
	}

	@Override
	public boolean getStatus() 
	{
		return state.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException 
	{	
		state.update();
		
		this.Notify();
	}

	@Override
	public double getValue() throws SensorNotActivatedException 
	{
		return state.getValue();
	}

	@Override
	public void attach(View v) 
	{
		if (!views.contains(v))
			views.add(v);
	}

	@Override
	public void detach(View v) 
	{
		if (views.contains(v))
			views.remove(v);
	}

	@Override
	public void Notify() 
	{
		for (View v : views)
			v.update();
	}
}
