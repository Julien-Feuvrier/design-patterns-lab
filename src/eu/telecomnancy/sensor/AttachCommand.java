package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.View;

/**
 * 
 * @author julien
 * apply sensor.attach(view);
 * 
 */
public class AttachCommand implements Command 
{
	private ISensor sensor;
	private View view;
	
	public AttachCommand(ISensor sensor, View view)
	{
		this.sensor = sensor;
		this.view = view;
	}
	@Override
	public void execute() 
	{
		sensor.attach(view);
	}
}
