package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.ui.View;

public class AdapterLegacyTemperatureSensor implements ISensor 
{
	private LegacyTemperatureSensor lts;
	private ArrayList<View> views;
	
	public AdapterLegacyTemperatureSensor()
	{
		lts = new LegacyTemperatureSensor();
		views = new ArrayList<View>();
	}
	
	@Override
	public void on() 
	{
		if (!lts.getStatus())	// si le sensor est éteint
		{
			lts.onOff();		// on l'allume
			
			Notify();
		}
	}

	@Override
	public void off() 
	{
		if (lts.getStatus())
		{
			lts.onOff();
			
			Notify();
		}
	}

	@Override
	public boolean getStatus() 
	{
		return lts.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException 
	{
		if (!lts.getStatus())
			throw new SensorNotActivatedException("Sensor not activated");
		else 
		{
			lts.onOff();	// on éteint
			lts.onOff();	// on le rallume pour créer un nouveau thread et une nouvelle valeur aléatoire
			
			Notify();
		}
	}

	@Override
	public double getValue() throws SensorNotActivatedException 
	{
		if (!lts.getStatus())
			throw new SensorNotActivatedException("Sensor not activated");
		else return lts.getTemperature();		
	}

	@Override
	public void attach(View v) 
	{
		if (!views.contains(v))
			views.add(v);
	}

	@Override
	public void detach(View v) 
	{
		if (views.contains(v))
			views.remove(v);
	}

	@Override
	public void Notify() 
	{
		for (View v : views)
			v.update();
	}
}
