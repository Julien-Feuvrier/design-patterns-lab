/**
 * 
 */
package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.View;

/**
 * @author julien
 * abstract class for the decorator template
 * the children classes can change the value return by getValue();
 *
 */
public abstract class DecoratorTemperatureSensor implements ISensor 
{
	protected ISensor sensor;
	
	public DecoratorTemperatureSensor()
	{
		this.sensor = new TemperatureSensor();
	}
	
	public DecoratorTemperatureSensor(ISensor sensor)
	{
		this.sensor = sensor;
	}
	
	@Override
	public void on() 
	{
		sensor.on();
	}

	@Override
	public void off() 
	{
		sensor.off();
	}

	@Override
	public boolean getStatus() 
	{
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException 
	{
		sensor.update();
	}

	@Override
	abstract public double getValue() throws SensorNotActivatedException;

	@Override
	public void attach(View v) 
	{
		sensor.attach(v);
	}

	@Override
	public void detach(View v) 
	{
		sensor.detach(v);
	}

	@Override
	public void Notify() 
	{
		sensor.Notify();
	}
}
