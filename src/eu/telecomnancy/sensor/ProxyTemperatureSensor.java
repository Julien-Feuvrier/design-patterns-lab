package eu.telecomnancy.sensor;

import java.util.Date;
import eu.telecomnancy.ui.View;


/**
 * 
 * @author julien
 * @brief sensor using a proxy pattern
 *
 */
public class ProxyTemperatureSensor implements ISensor 
{
	private ISensor sensor;
	
	public ProxyTemperatureSensor()
	{
		sensor = new TemperatureSensor();
	}
	
	public ProxyTemperatureSensor(ISensor sensor)
	{
		this.sensor = sensor;		
	}
	
	@Override
	public void on() 
	{
		sensor.on();
		
		displayLog("on()", null);
	}

	@Override
	public void off() 
	{
		sensor.off();
		
		displayLog("off()", null);
	}
	
	/**
	 * @brief display on the terminal the date, the method and the return value
	 */
	private void displayLog(String method, Object returnValue)
	{
		Date date = new Date();
		
		System.out.println("-> Log operation : on " + date + "  ,  method " + method + " was called " + (returnValue == null? "" : "  ,  and returned " + returnValue));
	}

	@Override
	public boolean getStatus() 
	{
		boolean status = sensor.getStatus();
		
		displayLog("getStatus()", status);
		
		return status;
	}

	@Override
	public void update() throws SensorNotActivatedException 
	{
		sensor.update();

		displayLog("update()", null);
	}

	@Override
	public double getValue() throws SensorNotActivatedException 
	{
		double value = sensor.getValue();
		
		displayLog("getValue()", value);
		
		return value;
	}

	@Override
	public void attach(View v) 
	{
		sensor.attach(v);
	}

	@Override
	public void detach(View v) 
	{
		sensor.detach(v);
	}

	@Override
	public void Notify() 
	{
		sensor.Notify();
	}
}
