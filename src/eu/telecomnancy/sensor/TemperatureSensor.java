package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Random;

import eu.telecomnancy.ui.View;

public class TemperatureSensor implements ISensor 
{
    boolean state;
    double value;
    private ArrayList<View> views;
    
    public TemperatureSensor()
    {
    	state = false;
    	value = (new Random()).nextDouble() * 100;
    	views = new ArrayList<View>();
    }

    @Override
    public void on() 
    {
        state = true;
        
        Notify();
    }

    @Override
    public void off() 
    {
        state = false;
        
        Notify();
    }

    @Override
    public boolean getStatus() 
    {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException 
    {
        if (state)
            value = (new Random()).nextDouble() * 100;
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        
        Notify();
    }

    @Override
    public double getValue() throws SensorNotActivatedException 
    {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

	@Override
	public void attach(View v) 
	{
		if (!views.contains(v))
			views.add(v);
	}

	@Override
	public void detach(View v) 
	{
		if (views.contains(v))
			views.remove(v);
	}

	@Override
	public void Notify() 
	{
		for (View v : views)
			v.update();
	}
}
