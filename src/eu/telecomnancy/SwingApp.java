package eu.telecomnancy;

import eu.telecomnancy.sensor.*;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp 
{
    public static void main(String[] args) 
    {
    	/*
    	// test of the round decorator sensor containing a proxy sensor
        ISensor sensor = new RoundDecoratorTemperatureSensor(new ProxyTemperatureSensor());
        new MainWindow(sensor);
        */
    	

    	// test of the factory method pattern with TemperatureSensorCreator
    	TemperatureSensorCreator creator = new TemperatureSensorCreator();
    	ISensor sensor = creator.FactoryMethod(TemperatureSensorCreator.ROUND_SENSOR);
    	new MainWindow(sensor);
    }
}
